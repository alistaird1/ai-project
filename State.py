class State:
    def __init__(self, pieces, blocks):
        self.pieces = pieces
        self.blocks = blocks

    def apply(self, move):
        for piece in self.pieces:
            if piece.pos[0] == move[0] and piece.pos[1] == move[1]:
                piece.pos[0] += move[0]
                piece.pos[1] += move[1]

