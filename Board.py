import Tile
class Board:
    directions = ['l', 'tl', 'tr', 'r', 'br', 'bl']
    moves = {'l':(-1,0), 'tl':(0,-1), 'tr':(1,-1), 'r':(1,0),
            'br':(0,1), 'bl':(-1,1)}
    tiles = [[None for i in range(7)] for j in range(7)]
    def __init__(self):
        r = -3
        for q in range(0, 4):
            self.new_tile(q,r)
        r = -2
        for q in range(-1, 4):
            self.new_tile(q,r)
        r = -1
        for q in range(-2, 4):
            self.new_tile(q,r)
        r = 0
        for q in range(-3, 4):
            self.new_tile(q,r)
        r = 1
        for q in range(-3, 3):
            self.new_tile(q,r)
        r = 2
        for q in range(-3, 2):
            self.new_tile(q,r)
        r = 3
        for q in range(-3, 1):
            self.new_tile(q,r)

    def new_tile(self, q, r):
        tile = Tile.Tile((q,r))
        self.tiles[r+3][q+3] = tile

    def move(self, t_from, t_to):
        tile_from = self.tiles[t_from[1]+3][t_from[0]+3]
        tile_to = self.tiles[t_to[1]+3][t_to[0]+3]
        tile_from.piece.pos = t_to
        tile_to.piece = tile_from.piece
        tile_from.piece = None

    def distance(self, start, end):
        q1 = start.pos[0]
        q2 = end.pos[0]
        r1 = start.pos[1]
        r2 = end.pos[1]
        return (abs(q1 - q2) + abs(q1 + r1 - q2 - r2) + abs(r1 - r2))/2
