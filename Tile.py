import Piece
import Block
class Tile:
    def __init__(self, pos):
        self.pos = pos
        self.piece = None
        self.block = None
    
    def add_piece(self, piece):
        self.piece = piece
        self.piece.pos = self.pos

    def add_block(self, block):
        self.block = block
        self.block.pos = self.pos
